#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef DK9000_C
#define DK9000_C

#include <time.h>
#include <math.h>
#include <windows.h>
#include <string.h>
#include <ctype.h>
#include <commctrl.h>
#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "..\inc\version.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wunused-value"

#define COM_OPEN 1
#define COM_CLOSE 2
#define COM_READ 4
#define CH_BEGIN_CARGO 0
#define CH_OPEN_GATE 1

CHAR DioString[0x100];


typedef struct{
UINT16 Id; // ����� ������
UINT16 point; // ����� ���������
BOOL isActive; // �������� ������������� ������
BOOL isVisual; // ����������� ������
UINT16 X,Y; // ��������� ������� � ���� �����
CHAR dt[0x20]; // ����/����� ������
CHAR cargo[0x8]; // ���-�� ������������ ��������� � �����
CHAR num[0x10]; // �������� ����� �����
CHAR scr[0x20]; // ������ ��� �����������
}Cargo, *pCargo;


int _get_param(int* port, int* baud , PCHAR dioCmd, PDWORD tmr, PBYTE number,  BOOL* debug, BOOL* quit,PCHAR IniFile)
{
static BOOL isFirst=FALSE;
CHAR ip[0x10]={0};  // ip ����� ioLogic
PCHAR CurDir;
int rc=0,cc;

CurDir=(PCHAR) malloc_w(0x200);
GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);

GetPrivateProfileString("dk9000","addr","$01M",ip,15,(LPCSTR)CurDir);
lstrcpy((LPSTR) dioCmd,(LPCSTR)ip);
lstrcat((LPSTR)dioCmd,(LPCSTR)"\r");

*number=(BYTE)GetPrivateProfileInt("scale","point",1,(LPCSTR)CurDir);

*port=(int)GetPrivateProfileInt("dk9000","port",1,(LPCSTR)CurDir);
*baud=(int)GetPrivateProfileInt("dk9000","speed",9600,(LPCSTR)CurDir);
*tmr=(DWORD)GetPrivateProfileInt("dk9000","timeout",1000,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("dk9000","debug",0,(LPCSTR)CurDir);

if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
*quit=(BOOL)GetPrivateProfileInt("dk9000","exit",0,(LPCSTR)CurDir);

if(!isFirst)
{
	wsprintf(DioString,"Debug DK-9000 Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) DioString );
	printf("������������ DK-9000 �� �����:\t%s\n",(PCHAR)CurDir);
	printf("COM ���� DK-9000:\t\t\t%d\n",(INT16) (*port));
	printf("�������� ������:\t\t\t%lu ���\n", (DWORD) *baud);
	printf("��� ������:\t\t\t\t%d\n", 8);
    printf("���� ���:\t\t\t\t%d\n", 1);
    printf("��������:\t\t\t\t%s\n", "None");
	printf("������� ������:\t\t\t\t%s\n",(PCHAR) dioCmd);
	printf("������� ������:\t\t\t\t%lu ms\n", *tmr);
	printf("���������� �������:\t\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);
	_closeConsole();
}
isFirst=*debug;
cc=0;
free_w(CurDir);
return rc;
}

INT16 _detectModule(PCHAR module, PCHAR name, PCHAR command, PINT16 lAnsw)
{
INT16 rc=-1,len,cc=0;

lstrcpy((LPSTR)module,(LPCSTR)"None");

if((len=lstrlen((LPCSTR)name)) < 5)
        return rc;
if(name[len-1]!=0xD)
        return rc;
name[len-1]=0;

lstrcpy((LPSTR)module,(LPCSTR)(name+3));

if(name[len-2]=='D')
        name[len-2]=0;

if(!lstrcmp((LPCSTR)(name+3),(LPCSTR)"7050"))
            cc=5;
if(!lstrcmp((LPCSTR)(name+3),(LPCSTR)"7060"))
            cc=5;
if(!lstrcmp((LPCSTR)(name+3),(LPCSTR)"DK-9000"))
            cc=6;
            switch(cc)
            {
                case 0: break;
                case 1: break;
                case 2: break;
                case 3: break;
                case 4: break;
                case 5: name[0]='@';name[3]='\r';name[4]=0;
                        lstrcpy((LPSTR)command,(LPCSTR)name);
                        *lAnsw=6;rc=lstrlen(command);
                        break;
                case 6: name[0]='$'; sprintf(name+3,"C\r");
                        lstrcpy((LPSTR)command,(LPCSTR)name);
                        *lAnsw=rc=lstrlen(name);
            }

return rc;
}

HANDLE CommOpen(int cport,unsigned long Spd,unsigned RxLen,unsigned TxLen)
{
static  DCB           dcb;
static  COMMTIMEOUTS  ct;
HANDLE rc;
char Config[0x30],pName[0x10];
int cc;
   memset((void *)&dcb,0,sizeof(dcb));
   dcb.DCBlength=sizeof(DCB);
   sprintf(Config,"baud=%lu parity=N data=8 stop=1",Spd);
   cc = BuildCommDCB(Config,&dcb);
   dcb.fNull=FALSE;

ct.ReadIntervalTimeout=2UL;
//   ct.ReadIntervalTimeout=0xFFFFFFFF;
/*
   ct.ReadTotalTimeoutMultiplier=ct.ReadTotalTimeoutConstant=1;
   ct.WriteTotalTimeoutMultiplier=ct.WriteTotalTimeoutConstant=1;
  */
ct.ReadTotalTimeoutMultiplier=2UL;
ct.WriteTotalTimeoutMultiplier=1UL;
ct.ReadTotalTimeoutConstant=ct.WriteTotalTimeoutConstant=5UL;
   sprintf(pName,"COM%d",cport);
   rc = CreateFile(pName,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
   dcb.fBinary=TRUE;
   dcb.fParity=dcb.fOutxCtsFlow=dcb.fOutxDsrFlow=FALSE;
   dcb.fDtrControl=DTR_CONTROL_DISABLE;
   dcb.fDsrSensitivity=FALSE;
   dcb.fRtsControl=RTS_CONTROL_DISABLE;
   dcb.EvtChar='\r';
   cc = SetCommState(rc,&dcb);
   cc = SetCommTimeouts(rc,&ct);
   cc = PurgeComm(rc,PURGE_TXCLEAR|PURGE_RXCLEAR);
   cc = SetCommMask(rc,EV_RXCHAR);
   cc = SetupComm(rc,RxLen,TxLen);
   return rc;
}

WORD  SendStr(HANDLE p,PCHAR Str)
{
  DWORD bc=0;
    WriteFile(p,(const void*)Str,(unsigned long)lstrlen(Str),(LPDWORD)&bc,NULL);
  return (WORD)bc;
}

int ReadStr(HANDLE port,BYTE *Str,DWORD tout)
{
DWORD bc=0, w,len_read=1L;
int i=0;
BYTE flags=0,error_count=0;
Str[i]=0;
w = GetTickCount();
	while(TRUE)
	{
	if((GetTickCount()-w)>=tout)
        {
        w = GetTickCount()-w;
        break;
        }
		ReadFile(port,(PVOID) &Str[i],len_read,&bc,NULL);
		if(bc)
            {
			i+=bc;
			Str[i]=0;
			if(Str[i-1]=='\r')
                {
                w = GetTickCount()-w;
				break;
                }
            }
	}
return (lstrlen((LPSTR)Str));
}

CHAR _send2term(int port,unsigned long bd, PCHAR cmd,PCHAR answ,DWORD to,PDWORD tout,BYTE c)
{
int n,rc=0;
DWORD t;
PCHAR _answ;
static HANDLE hPort=NULL;

if(!c)
    return 0;
t = GetTickCount();

if((c&COM_OPEN))
    {
    if(hPort==NULL)
        hPort =CommOpen(port,bd,0x100,0x100);
    if(hPort==INVALID_HANDLE_VALUE)
        hPort=NULL;
    }

if((hPort!=NULL) && (c&COM_READ))
    {
    rc = (int) SendStr(hPort,cmd);
    _answ = (PCHAR)malloc_w(0x400);
    n = ReadStr(hPort,(PBYTE) _answ,to);
    if(n){
        lstrcpy((LPSTR)answ,(LPCSTR)_answ);
        rc = answ[4];
        }
    else
        rc=0;
    free_w(_answ);
    }

if((c&COM_CLOSE))
    {
    if(hPort!=INVALID_HANDLE_VALUE)
        {
        CloseHandle(hPort);
        hPort=NULL;
        }
    }
*tout = (GetTickCount()-t);
return rc;
}

int _setCursor(int port,unsigned long bd,PCHAR cmd, PCHAR data, CHAR k, PUINT16 isX,PUINT16 isY)
{
PCHAR _cmd;
int rc=0;
DWORD t;

   switch(k)
   {

       case 0:;break;
       case 'H': (*isY)++; (*isY)&=0x7;break;
       case 'F': (*isY)--; if((*isY) > 7) (*isY)=0;break;
       case 'G': (*isX)++; if((*isX) > 20) (*isX)=20;break;
       case 'E': if((*isX) > 0) (*isX)--;break;
       default: rc=1;break;
   }
if(!rc)
    {
    _cmd = (PCHAR)malloc_w(0x400);
    wsprintf(_cmd,"%3.3sT%d%02X",cmd,*isY,*isX);
    if(data!=NULL)
        lstrcat(_cmd,data);
    lstrcat(_cmd,"\r");
    rc = _send2term(port,bd,_cmd,_cmd,100L,&t,COM_READ);
    free_w(_cmd);
    }
return rc;
}
int _typeCursor(int port,unsigned long bd,PCHAR cmd, UINT16 type)
{
PCHAR _cmd;
DWORD t;
int rc;
    _cmd = (PCHAR)malloc_w(0x40);
    wsprintf(_cmd,"%3.3s0030%u\r",cmd,type);
    rc = _send2term(port,bd,_cmd,_cmd,100L,&t,COM_READ);
    free_w(_cmd);
return rc;
}

int _deleteChar(int port,unsigned long bd,PCHAR cmd,PUINT16 isX,UINT16 isY)
{
UINT16 Y;
int cc=0;
Y = isY;

    _setCursor(port,bd,cmd, " ", 0, isX,&Y);
    if((*isX) > 0) (*isX)--;
    cc = _setCursor(port,bd,cmd, NULL, 0, isX,&Y);
/*
    cc = _setCursor(port,bd,cmd, NULL, 'G', isX,&Y);*/
return cc;
}

int _insertChar(int port,unsigned long bd,PCHAR cmd,CHAR k,PUINT16 isX,UINT16 isY)
{
UINT16 Y;
int cc=0;
Y = isY;

CHAR dat[2];

dat[0]=k;dat[1]=0;
    cc=_setCursor(port,bd,cmd, dat, 0, isX,&Y);
    (*isX)++;
    if((*isX) > 20)
        (*isX)=20;
    cc = _setCursor(port,bd,cmd, NULL, 0, isX,&Y);
return cc;
}

void _clearStr(int port,unsigned long bd,PCHAR cmd,UINT16 isX,UINT16 isY,UINT16 l)
{
CHAR clr[22];
    sprintf(clr,"%*.*s",l,l," ");

if(isY>7) isY=7;
_setCursor(port,bd,cmd,clr,0,&isX,&isY);
}

void _viewBW500(int port,unsigned long bd,PCHAR cmd, pCARGOINFO pc)
{
UINT16 X,Y;
PCHAR fmstr;
static time_t t=0UL;
struct tm *nt;
Y=0;X=0;

fmstr = malloc_w(0x100);
if(pc->isCargo==1)
    {
//---------------------------------------------
    pc->Sum_1=time(NULL)%100;
    pc->vSum_1=_subDW(pc->Sum_1,pc->pSum_1,99UL);
//---------------------------------------------
    _typeCursor(port,bd,cmd,0);
    _clearStr(port,bd,cmd,X,Y,10);
    sprintf(fmstr+80,"%.1f",(float)pc->Sum_1/10.);
    sprintf(fmstr,"%10.10s",fmstr+80);
    _setCursor(port,bd,cmd,fmstr,0,&X,&Y);
    Y=1;
    _clearStr(port,bd,cmd,X,Y,10);
    sprintf(fmstr+80,"%.1f",(float)pc->vSum_1/10.);
    sprintf(fmstr,"%10.10s",fmstr+80);
    _setCursor(port,bd,cmd,fmstr,0,&X,&Y);
//    _typeCursor(port,bd,cmd,2);
    }
else if(pc->isCargo==2)
    {
//---------------------------------------------
    pc->Sum_2=time(NULL)%100;
    pc->vSum_2=_subDW(pc->Sum_2,pc->pSum_2,99UL);
//---------------------------------------------
    _typeCursor(port,bd,cmd,0);
    X=11;
    _clearStr(port,bd,cmd,X,Y,10);
    sprintf(fmstr+80,"%.1f",(float)pc->Sum_2/10.);
    sprintf(fmstr,"%10.10s",fmstr+80);
    _setCursor(port,bd,cmd,fmstr,0,&X,&Y);
    Y=1;
    _clearStr(port,bd,cmd,X,Y,10);
    sprintf(fmstr+80,"%.1f",(float)pc->vSum_2/10.);
    sprintf(fmstr,"%10.10s",fmstr+80);
    _setCursor(port,bd,cmd,fmstr,0,&X,&Y);
//    _typeCursor(port,bd,cmd,2);
    }
    if(t!=time(NULL))
        {
        Y=7;
        t = time(NULL);
        _clearStr(port,bd,cmd,0,Y,20);
        nt = localtime(&t);
        sprintf(fmstr,"%02u:%02u:%02u",nt->tm_hour,nt->tm_min,nt->tm_sec);
        X = 10 - (lstrlen(fmstr)>>1);
        _setCursor(port,bd,cmd,fmstr,0,&X,&Y);
        }
free_w(fmstr);
}

UINT16 _getActive(pCargo pc)
{
UINT16 Idx=0;
    while(pc[Idx].Id!=0)
    {
    if(pc[Idx].isActive)
            break;
    Idx++;
    }
return Idx;
}

UINT16 _clearActive(pCargo pc)
{
    UINT16 Idx=0;
    while(pc[Idx].Id!=0)
    {
    pc[Idx].isActive=pc[Idx].isVisual=FALSE;
    Idx++;
    }
return Idx;
}

pCargo _emuCargo(pCargo pc,pCARGOINFO ci, CHAR k, PCHAR a)
{
static BYTE prev=0,Idx=0;
struct tm *nt;
static time_t Now;
time_t Laps;
int sLen;

if(pc==NULL)
{
    prev=0;
    Idx=0;
    ci->BeginCount;
}
else if(!prev)
    prev=1;

sLen = lstrlen((LPCSTR)a);

if((sLen==6) && (k=='S'))
    {
    if(ci->isCargo==1)
        {
        ci->forward1=ci->gate1up^=1;
        ci->BeginCount = ci->forward1 | ci->gate1up;
        }
    else if(ci->isCargo==2)
        {
        ci->forward2=ci->gate2up^=1;
        ci->BeginCount = ci->forward2 | ci->gate2up;
        }

    }

    if(R_TRIG32(CH_OPEN_GATE,ci->BeginCount))
        {
        if(ci->isCargo==1)
            ci->pSum_1=ci->Sum_1;
        else if(ci->isCargo==2)
            ci->pSum_2=ci->Sum_2;

        Now = time(NULL);
        nt = localtime(&Now);
        sprintf(pc[Idx].dt,"%02d.%02d.%04d %02u:%02u:%02u",nt->tm_hour,nt->tm_mon+1,nt->tm_year+1900,nt->tm_hour,nt->tm_min,nt->tm_sec);
        sprintf(pc[Idx].num,"%d",Idx+1);
        pc[Idx].Id = Idx+1;
        pc[Idx].point=ci->isCargo;
        _clearActive(pc);
        pc[Idx].isActive=TRUE;
        }
    if(F_TRIG32(CH_OPEN_GATE,ci->BeginCount))
        {
/*        if(ci->isCargo==1)
            ci->pSum_1=ci->Sum_1;
        else if(ci->isCargo==2)
            ci->pSum_2=ci->Sum_2;*/
        if(Idx<0x80)
            Idx++;
        }
        if(ci->BeginCount)
            {
            if(ci->isCargo==1)
                sprintf(pc[Idx].cargo,"%02.1f",(float)ci->vSum_1/10.);
            else if(ci->isCargo==2)
                sprintf(pc[Idx].cargo,"%02.1f",(float)ci->vSum_2/10.);
            }
return pc;
}


pCargo _editCargo(int port,unsigned long bd,PCHAR cmd,CHAR k, PCHAR a, pCargo pc)
{
static UINT16 Win=0;
UINT16 count=0,isX=0,Idx=0;
INT16 isAct=-1,i;
BOOL bc,kc;
pCargo cc=NULL;
int kLen;

    if(pc==NULL)
        {
        count = 0; isX=0; Win=0;
        return NULL;
        }

if((count = _getActive(pc))>3)
    {
    if((count-3)>Win)
        Win=count-3;
    else
        count=0;
    }
else
    count=0;

        for(i=0;i<4;i++)
            if((Win+i+3)< 200)
            {
            pc[Win+i].Y=i+3;
            if(!pc[Win+i].X)
                pc[Win+i].X=14;
            if(count)
                pc[Win+i].isVisual=FALSE;
            }

    while((pc[Win+Idx].Id!=0) && (Idx<4))
    {
// ����������� ������ ����������� ������
    if((!pc[Win+Idx].isVisual) ||(pc[Win+Idx].isActive))
        {
        sprintf(pc[Win+Idx].scr,"%02u%c%5.5s %4.4s %-7.7s",pc[Win+Idx].Id,pc[Win+Idx].isActive? '>' : ' ',pc[Win+Idx].dt+11,pc[Win+Idx].cargo,pc[Win+Idx].num);
        _setCursor(port,bd,cmd,pc[Win+Idx].scr,0,&isX,&pc[Win+Idx].Y);
        pc[Win+Idx].isVisual=TRUE;
        if(!pc[1].Id)
            {
            for(i=1;i<4;i++)
                _clearStr(port,bd,cmd,0,i+3,20);
            }
        }
    if(pc[Win+Idx].isActive)
        {
        isAct=Win+Idx;
        _typeCursor(port,bd,cmd,2);
        }

    Idx++;
    }
// ��������� ������ ������������ �������
    if(isAct > -1)
        {
        if((k=='G') && (pc[isAct].X<20))
                pc[isAct].X++; // ��� �����
        if((k=='E') && (pc[isAct].X>14))
                pc[isAct].X--; // ��� ������
        if((k=='H') && (pc[isAct+1].Id)) // ��� ����
            {
            _clearActive(pc);
            isAct++;
            pc[isAct].isActive=TRUE;
            if(isAct>3)
                Win++;

            }
        if((k=='F') && (pc[isAct-1].Id)) // ��� �����
            {
            if(isAct)
                {
                _clearActive(pc);
                pc[isAct-1].isActive=TRUE;
                if((Win) && (pc[isAct].Y<4))
                    Win--;
                isAct--;
                }
            }
        _setCursor(port,bd,cmd,NULL,0,&pc[isAct].X,&pc[isAct].Y); // �������� ������
        cc = pc+isAct;
        }
//______________________________________

// ��������� �������� ������
bc =  (kLen=lstrlen((LPCSTR) a)==6);
kc = bc && ((k>'/') && (k<':'));
if(kc)
    {
        kLen = pc[isAct].X-14;
        pc[isAct].num[kLen]=k;
        if(pc[isAct].X<20)
            pc[isAct].X++;
        pc[isAct].isVisual=FALSE;
    }
//___________________________

// ��������� ������� DEL
kc = bc && (k=='J');
if(kc)
    {
    kLen = pc[isAct].X-14;
    if(!pc[isAct].num[kLen+1])
        pc[isAct].num[kLen]=0;
    else
        pc[isAct].num[kLen]=' ';
    if(pc[isAct].X>14)
        pc[isAct].X--;
    pc[isAct].isVisual=FALSE;
    }
//______________________________
return cc;
}

pCargo _initCargo(CHAR k,pCARGOINFO pc)
{
static pCargo cc = NULL;
struct tm *nt;
time_t Now;
BOOL pt_1,pt_2;

pt_1 = (k=='B') && (pc->isCargo==1) && (cc!=NULL);
pt_2 = (k=='D') && (pc->isCargo==2) && (cc!=NULL);

if(((k=='A')||(k=='C')) && cc==NULL) // ��������� F1,F3 (������ ��������)
            {
             if((cc = (pCargo) malloc_w(sizeof(CARGOINFO)*0x100))!=NULL)
                {
                if(k=='A')
                    cc[0].point=1;
                if(k=='C')
                    cc[0].point=2;
                cc[0].isActive=TRUE;
                cc[0].Id=1;
                pc->isCargo=cc[0].point;
                if(pc->isCargo==1)
                    {
                    pc->Sum_1=pc->pSum_1=time(NULL)%100;
                    pc->vSum_1=_subDW(pc->Sum_1,pc->pSum_1,99UL);
                    }
                else if(pc->isCargo==2)
                    {
                    pc->Sum_2=pc->pSum_2=time(NULL)%100;
                    pc->vSum_2=_subDW(pc->Sum_2,pc->pSum_2,99UL);
                    }
                Now=time(NULL);
                nt = localtime(&Now);
                wsprintf(pc->fname,".\\%02d\\%02d%02u%02u%02u%02u.xml",pc->ID,nt->tm_year-100,nt->tm_mon+1,nt->tm_mday,nt->tm_hour,nt->tm_min);
                }
            }

else if((pt_1) || (pt_2)) // ��������� F2,F4 (����� ��������)
            {
                pc->fname[0]=0;
                free_w(cc);
                pc->isCargo=0;
                cc = NULL;
            }
return cc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
{
    DWORD isNow;
    int cport, baud; // ��� ���� � ��������
    CHAR iCmd[0x40]={0}; // ������� ������ ������
    CHAR Answ[0x40]; // ����� ������
    CHAR modname[0x10];
    CHAR c,key;
    BYTE num; // ����� �����
    DWORD timeout; // ������� ������ � �������������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������
	BOOL connection=FALSE,sendCmd=FALSE; // ���� ������ ����������
    int cc,n,count=0,dcount=0;
    INT16 mLen=-1; //����� ������� � ������
    INT16 aLen=-1; //����� ������ �� ������
	UINT16 CoordX=0,CoordY=0,Data=1;
	FileMap fm;
    pCARGOINFO pCi;
    PBYTE dataExchange;
    CHAR cfg[0x20];
	HANDLE isRun,hPort;
	DWORD to=50UL,Laps;
	pCargo ptr_cargo=NULL;


if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
    return FALSE;

cc = _get_param(&cport, &baud,iCmd, &timeout, &num, &debug, &quit,(PCHAR)cfg);
debug=0;

wsprintf((LPSTR)DioString,"Local\\DK-9000%03d",num);
    if((isRun = _chkIsRun((LPSTR)DioString))==NULL)
    {
		wsprintf(DioString,"Debug DK-9000 Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) DioString );
        printf("��������� DK9000 -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
//    _closeConsole();
    return FALSE;
    }

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
pCi=(pCARGOINFO)fm.dataPtr;

if(pCi->ID!=num)
	InterlockedExchange((volatile PDWORD) &pCi->ID,(DWORD)num);

cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_OPEN|COM_READ);
if(cc)
    mLen=_detectModule((PCHAR) modname,(PCHAR) Answ, (PCHAR) iCmd, (PINT16) &aLen);


if(debug && cc)
	printf("COM port Init:%d\n",cport);


isNow=(DWORD)time(NULL);
_closeConsole();
debug=0;

R_TRIG32(CH_BEGIN_CARGO,TRUE);
F_TRIG32(CH_BEGIN_CARGO,FALSE);
R_TRIG32(CH_OPEN_GATE,TRUE);
F_TRIG32(CH_OPEN_GATE,FALSE);
Laps=GetTickCount();
    while(!quit)
    {

        if(_chkConfig((LPSTR) cfg)){
			cc = _get_param(&cport, &baud, iCmd, &timeout, &num, &debug, &quit,(PCHAR)cfg);
            if(cc & 0x2){
                if(debug){
					sprintf(DioString,"Debug %s Console: %s",modname,SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) DioString );
                    printf("%s ��� ����:%u\n",modname,cport+1);
                }
                else
					_closeConsole();
            }
            if(!quit)
                {
                cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_CLOSE);
                cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_OPEN|COM_READ);
//                wsprintf(iCmd+3,"00301\r");
//                cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to);
                if(cc){
                        mLen=_detectModule((PCHAR) modname,(PCHAR) Answ, (PCHAR) iCmd, (PINT16) &aLen);
//                        _setCursor(cport,(unsigned long) baud,iCmd, "123456789012345678901", 0,&CoordX,&CoordY);
//                        wsprintf(iCmd+3,"C\r");
                        cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_READ);
                        //_setCursor(cport,(unsigned long) baud,iCmd, NULL, 0,&CoordX,&CoordY);
                    }
                else if(debug)
                    printf("%s ������ �������� ��� �����:%u\n",modname,cport);

                }
                else
                    debug=0;
            }


            wsprintf(iCmd+3,"K\r");
            cc = _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_READ);
            if(cc=='\r')count=0;
            n = lstrlen(Answ);
            key = Answ[n-2];
            if(key=='I') {
                quit=1;
            wsprintf(iCmd+3,"C\r");
            _send2term(cport,(unsigned long) baud, iCmd, Answ,100UL,&to,COM_READ);
            }

            ptr_cargo = _initCargo(key,pCi);

            if(R_TRIG32(CH_BEGIN_CARGO,(BYTE) (ptr_cargo==NULL)))
                _typeCursor(cport,baud,iCmd,0);
            if(F_TRIG32(CH_BEGIN_CARGO,(BYTE) (ptr_cargo!=NULL)))
                {
                _typeCursor(cport,baud,iCmd,0);
                for(n=3;n<8;n++)
                    _clearStr(cport,baud,iCmd,0,n,21);
                }

            _emuCargo(ptr_cargo,pCi,key,Answ);
            _viewBW500(cport,(unsigned long) baud,iCmd,pCi);
//Laps=GetTickCount()-Laps;
            _editCargo(cport,(unsigned long) baud,iCmd,key,Answ,ptr_cargo);

   	if((debug && (((DWORD)time(NULL)-isNow) >= 1)) || (n==6))
			{
            if (cc)
                printf("%lu| %s | %4.4s |%c | %d | %lu\n",(unsigned long) isNow,modname,iCmd,Answ[n-2],n,to);
            else
                printf("%lu| ������ ����� � ������� %s\n",(unsigned long) isNow,modname);
			isNow=time(NULL);
			}
    Sleep(50UL);
//Laps=GetTickCount();
    }
if(!debug)
	_closeConsole();
	 _send2term(cport,(unsigned long) baud, iCmd, Answ,20UL,&to,COM_CLOSE);
/*    _setFlags(&pCri->Flags, LOSTCONNECT, 1);
	InterlockedExchange8((PBYTE) &pWi->lnkLogik,0);
	InterlockedExchange8((PBYTE) &pWi->Bell,0);
	InterlockedExchange8((PBYTE) &pWi->Light,0);
	InterlockedExchange8((PBYTE) &pWi->Semaphore,0);
	CloseHandle(isRun);*/
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // DK9000_C
