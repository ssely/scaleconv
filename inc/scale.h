#include "version.h"

#pragma pack (push,1)
typedef struct{
BYTE ID; //����� �����
BYTE isLink;// ����� � ��������
BYTE isCargo;// ��� ��������
BYTE BeginCount; // ������ �������� � ������
long Load; // ��������
long Rate; // ������������������
DWORD Speed; // ��������
DWORD Sum_1; // �������� 1
DWORD Sum_2; // �������� 2
DWORD pSum_1; // ���������� ��������� ��������� 1
DWORD pSum_2; // ���������� ��������� ��������� 2
DWORD vSum_1; // Sum_1-pSum_1 = �������� ��������� 1
DWORD vSum_2; // Sum_2-pSum_2 = �������� ��������� 2
BYTE gate1up,gate2up,forward1,forward2; // ����� ��������� ������� � ����������
CHAR dtstr[0x14];
CHAR fname[0x80]; // ��� ����� ��� ����������
//CHAR dkstring[8][21];
} CARGOINFO, *pCARGOINFO;

typedef struct{
  HANDLE hFile; // ����������  �����
  HANDLE hMapping; // ����������  ����� � ������
  size_t fsize;  // ������ ����� � ������
  PBYTE dataPtr; // ��������� �� ������ ��� ����������� ����������� �����
} FileMap, *pFileMap;

#pragma pack (pop)

#define malloc_w(m) HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (m))
#define free_w(m)  HeapFree(GetProcessHeap(),0,(LPVOID) (m))

#ifdef SCALE_C_
CARGOINFO WI={0};
DWORD CarWeight,tCycle;
extern struct mg_mgr mgr;
extern int server(char *, char * );
#endif // SCALE_C_
#ifndef MISC_C
extern PVOID _createExchangeObj(int, pFileMap);
extern void _destroyExchangeObj(int, pFileMap);
extern BOOL _chkConfig(LPSTR );
extern int  _find_cmd_variable(PCHAR, PCHAR, PCHAR);
extern void _openConsole(LPSTR);
extern void _closeConsole(void);
extern void WriteLogWeb(LPSTR,int,BOOL);
extern BYTE InterlockedExchange8(CHAR volatile *, CHAR );
extern SHORT InterlockedExchange16(SHORT volatile *, SHORT Value);
extern BYTE R_TRIG32(BYTE,BYTE);
extern BYTE F_TRIG32(BYTE,BYTE);
extern INT  _DeleteLog(int ,int );
extern HANDLE _chkIsRun(LPSTR);
extern BOOL isNoLogin(LPSTR);
extern UINT16 hex2dec(PCHAR);
extern DWORD _subDW(DWORD,DWORD,DWORD);
#endif //MISC_C
