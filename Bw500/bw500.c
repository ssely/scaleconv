#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#ifndef EXHANGE_C
#define EXHCANGE_C


#include <stdio.h>
#include <math.h>
#include <time.h>
#include <winsock2.h>
#include <windows.h>
#include "..\inc\version.h"
#include "..\inc\modbus.h"
#include "..\inc\scale.h"

#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wpointer-sign"

#define VT_WEIGHT 1
#define VT_ZERO 2

#define BusyStat 8 // ����� ��������� �����
#define KeyStat 9 // �������� ������ ���������

CHAR ReqString[0x100];

//--------------

int _find_chr(PBYTE Buff, CHAR chr)
{
int i,cc=0;
    for(i=0;i<6;i++)
        if(Buff[i]==chr)
	    {
    	    cc = i+1;
        	break;
	    }
return cc;
}

INT32 _swapW(INT32 *data, int count)
{
int i;
        for(i=0;i<count;i++)
            data[i]=(((data[i] & 0xFFFF) << 16) | ((data[i] & 0xFFFF0000) >> 16));

return data[count-1];
}

modbus_t * mb_close(modbus_t *mb)
{
if(mb!=(modbus_t *)NULL)
	{
	modbus_close(mb);
	modbus_free(mb);
	}
mb=NULL;
return mb;
}

modbus_t * mb_connect( char port,int speed,char parity,int data, int stop,int slave)
{
PCHAR com;
modbus_t * mb;
int len_str;

com = malloc_w(0x10);
wsprintf((LPSTR) com,"\\\\.\\COM0");
len_str = lstrlen(com);
com[len_str-1]=port;
mb=modbus_new_rtu(com,speed,parity,data,stop);
	if (modbus_connect(mb) == -1)
		{
	    modbus_free(mb);
		mb=(modbus_t *)NULL;
		free_w(com);
    	return mb;
		}
modbus_set_slave(mb,slave);
free_w(com);
return mb;
}

void _setBW500time(modbus_t *mb,pCARGOINFO p)
{
    INT16 *settime;
	struct tm *nt;
	time_t Now;
    int cc;
    Now=time(NULL);
    nt = localtime(&Now);
    settime = (INT16 *)malloc_w(0x10);
    settime[0]=nt->tm_year+1900;
    settime[1]=nt->tm_mon+1;
    settime[2]=nt->tm_mday;
    settime[3]=nt->tm_hour;
    settime[4]=nt->tm_min;
    settime[5]=nt->tm_sec;
    cc = modbus_write_registers(mb, 999, 6, (const uint16_t *) settime);

    if(cc == 6)
    {
    cc = modbus_read_registers(mb,999 ,6, (uint16_t *) settime );
    if(p!=NULL)
        sprintf(p->dtstr,"%02u:%02u:%02u %02u.%02u.%04u",settime[3],settime[4],settime[5],settime[2],settime[1],settime[0]);
    }

    free_w(settime);
}

INT16 _build_list(PBYTE dst, PCHAR src)
{
    INT16 Idx=0,beg=0,ptr=0;

    if(isdigit(src[0]))
        dst[Idx++]=atoi(src+ptr);
    dst[Idx]=0;
    while((beg = _find_chr((PBYTE) src+ptr, ',')))
          {
            ptr+=beg;
            if(isdigit(src[ptr]))
                dst[Idx++]=atoi(src+ptr);
            dst[Idx]=0;
          }
return Idx;
}

int _get_param(PBYTE port, PBYTE addr,int *spd, PBYTE dat,PBYTE stp,PCHAR par,PCHAR num,PDWORD timeout, BOOL* debug, BOOL* quit,PCHAR IniFile)
{
CHAR CurDir[0x100];

static BOOL isFirst=FALSE;
int rc=0,cc;
PCHAR tmp;

GetCurrentDirectory(0x100,CurDir);
lstrcat((LPSTR)CurDir,"\\");
lstrcat((LPSTR)CurDir,(LPCSTR)IniFile);

*port=(BYTE)GetPrivateProfileInt("bw500","port",1,(LPCSTR)CurDir)+0x30;
tmp = (PCHAR)malloc_w(0x100);
GetPrivateProfileString("bw500","addr","1",tmp,0xFF,(LPCSTR)CurDir);
cc = _build_list(addr, tmp);

//*addr=(BYTE)GetPrivateProfileInt("bw500","addr1",1,(LPCSTR)CurDir);
*spd=(int)GetPrivateProfileInt("bw500","speed",9600,(LPCSTR)CurDir);
*dat=(BYTE)GetPrivateProfileInt("bw500","data",8,(LPCSTR)CurDir);
*stp=(BYTE)GetPrivateProfileInt("bw500","stop",1,(LPCSTR)CurDir);
*num=(BYTE)GetPrivateProfileInt("bw500","point",1,(LPCSTR)CurDir);
cc = (BYTE)GetPrivateProfileInt("bw500","parity",0,(LPCSTR)CurDir);
if(!cc)
    par[0]='N';
if(cc==1)
    par[0]='O';
if(cc==2)
    par[0]='E';
*timeout=(DWORD)GetPrivateProfileInt("bw500","timeout",5000,(LPCSTR)CurDir);
cc=GetPrivateProfileInt("bw500","debug",0,(LPCSTR)CurDir);
if(*debug != (BOOL)cc)
    {
    rc|=2;
    *debug = (BOOL)cc;
    }
if(!isFirst)
{
	wsprintf(ReqString,"Debug ModBus Request Console: %s",SC_UBUNTU_FULLVERSION);
	_openConsole((LPSTR) ReqString );
	printf("������������ ModBus BW500 �� �����: %s\n",(PCHAR)CurDir);
    printf("����� ��������:\t\t\t%d\n",(INT) *num);
	printf("Modbus ����� �������:\t\t%s\n",tmp);
	printf("���� ������:\t\t\tCOM%c\n",*port);
	printf("�������� ������:\t\t%d ���\n",(INT) *spd);
	printf("��� ������:\t\t\t%d\n", *dat);
	printf("����-��� ������:\t\t%d\n", *stp);
	printf("��������:\t\t\t%c\n", par[0]);
	printf("������� �������� ������:\t%lu ms\n", *timeout);
	printf("���������� �������:\t\t%s\n", *debug ? "���" : "����");
	Sleep(5000UL);

}
free_w(tmp);

if(!(isFirst=*debug))
		_closeConsole();

*quit=(BOOL)GetPrivateProfileInt("bw500","exit",0,(LPCSTR)CurDir);
return rc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdline, int nCmdShow)
//int main()
{
    DWORD BusyTick,summ=0UL;
    BYTE port; // ����� ��� �����
//	BYTE addr; //����� ������� BW500 ��� ������ �� �������
	PCHAR addr_list;
	BYTE data; // ��� ������
	BYTE stop; // ����-���
	BYTE parity[4]; //��������
	BYTE num; //����� ����� �����
	int speed; // �������� ������
    int dataport;  // ���� ��� ��������� ������ � Modbus
    DWORD timeout; //������� �������� ������ � ����� � �������������
    BOOL debug=FALSE; //������/������� (1/0) ����������� ����
    BOOL quit=0; // ���������� �������� ������

    int cc,ErrorLink=0,Idx=0;
    FileMap fm;
	pCARGOINFO ci;
    PBYTE dataExchange;
    CHAR cfg[0x40];
	HANDLE isRun;
    modbus_t *ctx;
    INT32 conv[5];
    WORD Mode=0xFFFF;

	time_t isNow,dbgNow;


if(!(cc = _find_cmd_variable((PCHAR)lpszCmdline,"conf" , (PCHAR) cfg)))
 		return FALSE;


addr_list=(PCHAR)malloc_w(0x100);
cc=_get_param(&port,addr_list,&speed,&data, &stop, parity,&num,&timeout,&debug,&quit,cfg);
debug=0;

wsprintf((LPSTR)ReqString,"Local\\Bw500%03d",num);
if((isRun = _chkIsRun((LPSTR)ReqString))==NULL)
    {
		wsprintf(ReqString,"Debug ModBus RTU BW500 Console: %s",SC_UBUNTU_FULLVERSION);
		_openConsole((LPSTR) ReqString );
        printf("��������� bw500 -conf=%s  ��� ��������. �������...",cfg);
    Sleep(5000UL);
    _closeConsole();
    free_w(addr_list);
    return FALSE;
    }

if(debug)
	printf("Mutex %s is open\n",ReqString);

dataExchange=(PBYTE)_createExchangeObj(num, &fm);
ci=(pCARGOINFO)fm.dataPtr;

if(debug)
	printf("Exchange Object is open:%p\n",dataExchange);

if(ci->ID!=num)
     InterlockedExchange8((PBYTE) &ci->ID,(BYTE)num);

if((ctx=mb_connect(port,speed,parity[0],data,stop,addr_list[0]))==NULL)
{
    if(debug)
        printf("%lu | Modbus �����:%2u | ��� ����������\n",(DWORD)time(NULL),num);
    InterlockedExchange8((PBYTE) &ci->isLink,0);
	CloseHandle(isRun);
	free_w(addr_list);
    _destroyExchangeObj(num, &fm);
    return FALSE;
}
cc = modbus_read_registers(ctx,61 , 1, (uint16_t *) &Mode);
cc = modbus_read_registers(ctx,1009 , 10, (uint16_t *) conv);
if(!Mode)
    _swapW(conv,5);

printf("%02d:Load:\t\t%d\n",addr_list[Idx],conv[0]);
printf("Rate:\t\t\t%d\n",conv[1]);
printf("Speed:\t\t\t%lu\n",(DWORD)conv[2]);
printf("Sum_1:\t\t\t%lu\n",(DWORD)conv[3]);
printf("Sum_2:\t\t\t%lu\n",(DWORD)conv[4]);
isNow=time(NULL);

while(!quit)
    {
        if(_chkConfig((LPSTR) cfg)){
            cc=_get_param(&port,addr_list,&speed,&data, &stop,parity,&num,&timeout,&debug,&quit,cfg);
            if(cc & 0x2){
                if(debug){
                    sprintf(ReqString,"Debug ModBus BW500 Console: %s",SC_UBUNTU_FULLVERSION);
					_openConsole((LPSTR) ReqString );
                    printf("ModBus �����:%d  ModBus ���� :%c\n",addr_list[Idx],port);
                }
                else
					_closeConsole();
            }
            ctx = mb_close(ctx);
        }

        cc=0;
        if(ctx!=NULL)
        {
            cc = modbus_read_registers(ctx,61 , 1, (uint16_t *) &Mode);
            cc = modbus_read_registers(ctx,1009 , 10, (uint16_t *) conv);
            if(!Mode)
                _swapW(conv,5);

            dbgNow = time(NULL)-isNow;
            if(dbgNow>0)
                {
                if(cc ==10)
                    {
                    printf("%02d:Load: %d|",addr_list[Idx],conv[0]);
                    printf("Rate: %d|",conv[1]);
                    printf("Speed: %lu|",(DWORD)conv[2]);
                    printf("Sum_1: %lu|",(DWORD)conv[3]);
                    printf("Sum_2: %lu\n",(DWORD)conv[4]);
                    _setBW500time(ctx,ci);
                    printf("Local Time: %s\n",ci->dtstr);
                    }
                isNow=time(NULL);
                }
        }
        if(cc!=10)
            {
            mb_close(ctx);
			ctx=mb_connect(port,speed,parity[0],data,stop,addr_list[Idx]);
			isNow=time(NULL);
			if(debug)
                printf("%lu.\t%02d:Reconnect...\n",(unsigned long)isNow,addr_list[Idx]);
			ErrorLink++;
			Idx=0;
			Sleep(2000UL);
			continue;
            }
        else
            ErrorLink=0;
//		InterlockedExchange8((PBYTE) &wi->Motion,_isMove((long) weg_1, 10UL, 500UL));
		if(ErrorLink<3)
            InterlockedExchange8((PBYTE) &ci->isLink,1);
        else
            InterlockedExchange8((PBYTE) &ci->isLink,0);

    if(!addr_list[Idx+1])
        Idx=0;
    else
        Idx++;
    modbus_set_slave(ctx,addr_list[Idx]);
    Sleep(200UL);
	}

	//if(!debug)
        _closeConsole();
    free_w(addr_list);
	mb_close(ctx);
	InterlockedExchange8((PBYTE) &ci->isLink,0);
	CloseHandle(isRun);
    _destroyExchangeObj(num, &fm);
    return cc;
}
#endif // EHCHANGE_C
